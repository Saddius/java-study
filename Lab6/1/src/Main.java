import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;
import java.io.File;

/**
 * Created by Денис on 25.04.2016.
 */
public class Main {
    private static ArrayList<File> listWithFileNames = new ArrayList<>();
    private static String name;

    public static void main(String[] args) {
        System.out.println("Введите название файла или часть его названия:");
        Scanner nameOfFile = new Scanner(System.in);
        name = nameOfFile.nextLine();
        String ourPath = "d:\\javastudy\\java-study";
        getListFiles(ourPath);
        int i = 1;
        System.out.println("Это список похожих файлов:");

        for (File fil : listWithFileNames) {
            System.out.println(fil.getName());
            Path x = fil.toPath();
            Path start = Paths.get(ourPath);
            String middle = x.subpath(start.getNameCount(), start.getNameCount() + 1).toString() + i;
            File newPath = new File(ourPath + "\\" + middle + "\\" + x.subpath(start.getNameCount() + 1, x.getNameCount() - 1));
            newPath.mkdirs();
            i++;
            File y = new File(newPath + "\\" + x.subpath(x.getNameCount() - 1, x.getNameCount()));
            try {
                Copier Copier = new Copier(x, y.toPath());
                Copier.start();
            } catch (Exception e) {
            }
        }
    }

    public static void getListFiles(String str) {
        File f = new File(str);
        for (File s : f.listFiles()) {
            if (s.isFile()) {
                if (s.getName().contains(name)) {
                    listWithFileNames.add(s);
                }
            } else if (s.isDirectory()) {
                getListFiles(s.getAbsolutePath());
            }
        }

    }
}





