import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Created by saddius on 4/27/2016.
 */
public class Copier extends Thread {

    Path x;
    Path y;

    public Copier(Path filePath, Path rootPath) {
        this.x = filePath;
        this.y = rootPath;
    }

    @Override
    public void run() {

        try {
            Files.copy(x, y);
        } catch (IOException e) {
        }
    }
}
