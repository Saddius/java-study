package Lab5;

import java.lang.reflect.Array;
import java.util.*;


/**
 * Created by Денис on 20.04.2016.
 */
public class myList implements List {

    private Object[] elementData = new Object[30];
    public int count; //счетчик заполненных элементов массива

    @Override
    public int size() {

        return elementData.length;
    }

    @Override
    public boolean isEmpty() {

        return elementData.length == 0;
    }

    @Override
    public boolean contains(Object o) {
        return indexOf(o) != -1;
    }

    @Override
    public Iterator iterator() {
        return new Itr();
    }

    @Override
    public Object[] toArray() {
        Object[] newArr = new Object[elementData.length];
        System.arraycopy(elementData, 0, newArr, 0, elementData.length);
        return newArr;
    }

    @Override
    public Object[] toArray(Object[] a) {
        if (elementData.length <= a.length) {
            System.arraycopy(elementData, 0, a, 0, elementData.length);
            return a;
        } else {
            return elementData;
        }
    }

    @Override
    public boolean add(Object o) {
        count = -1;
        //считаем колличество заполненных элементов
        for (Object y : elementData) {
            if (y != null) {
                count++;
            }
        }
        if (elementData[elementData.length - 1] == null) {
            elementData[count + 1] = o;
        } else {
            Object[] tempArray = new Object[elementData.length * 2];
            System.arraycopy(elementData, 0, tempArray, 0, elementData.length);
            elementData = tempArray;
            elementData[count + 1] = o;
        }
        return true;
    }

    @Override
    public boolean remove(Object o) {
        int num = -1;
        for (int i = 0; i < elementData.length; i++)
            if (elementData[i].equals(o)) {
                num = i;
                Object[] newArr = new Object[elementData.length - 1];
                System.arraycopy(elementData, 0, newArr, 0, i);
                System.arraycopy(elementData, i + 1, newArr, i, elementData.length - i - 1);
                elementData = newArr;
            }
        return elementData[num].equals(o);
    }

    @Override
    public boolean containsAll(Collection c) {
        int newSize = c.size();
        for (Object each : c) {
            for (int i = 0; i < elementData.length; i++) {
                if (each.equals(elementData[i])) {
                    newSize--;
                    break;
                }
            }
        }
        if (newSize > 0) {
            return false;
        }
        return true;
    }

    @Override
    public boolean addAll(Collection c) {
        Object[] newArr = new Object[elementData.length + c.size()];
        System.arraycopy(elementData, 0, newArr, 0, elementData.length);
        System.arraycopy(c, 0, newArr, elementData.length, c.size());
        elementData = newArr;
        return true;
    }

    @Override
    public boolean addAll(int index, Collection c) {
        Object[] newArr = new Object[elementData.length + c.size()];
        System.arraycopy(elementData, 0, newArr, 0, index);
        System.arraycopy(c, 0, newArr, index, c.size());
        System.arraycopy(elementData, index, newArr, index + c.size(), elementData.length - index);
        elementData = newArr;
        return false;
    }

    @Override
    public boolean removeAll(Collection c) {
        for (Object each : c) {
            for (int i = 0; i < elementData.length; i++) {
                if (each.equals(elementData[i])) {
                    Object[] newArr = new Object[elementData.length - 1];
                    System.arraycopy(elementData, 0, newArr, 0, i);
                    System.arraycopy(elementData, i + 1, newArr, i, elementData.length - i);
                    elementData = newArr;
                }
            }
        }
        return true;
    }

    @Override
    public boolean retainAll(Collection c) {
        Object[] newArr1 = new Object[0];
        for (Object each : c) {
            for (int i = 0; i < elementData.length; i++) {
                if (each.equals(elementData[i])) {
                    Object[] newArr2 = new Object[newArr1.length + 1];
                    System.arraycopy(newArr1, 0, newArr2, 0, newArr1.length);
                    newArr2[newArr1.length] = elementData[i];
                    newArr1 = newArr2;
                }
            }
        }
        elementData = newArr1;
        return true;
    }

    @Override
    public void clear() {
        Object[] newArr = new Object[0];
        elementData = newArr;
    }

    @Override
    public Object get(int index) {
        return elementData[index];
    }

    @Override
    public Object set(int index, Object element) {
        elementData[index] = element;
        return elementData;
    }

    @Override
    public void add(int index, Object element) {
        if (index < elementData.length) {
            Object[] newArr = new Object[elementData.length + 1];
            System.arraycopy(elementData, 0, newArr, 0, index);
            newArr[index] = element;
            System.arraycopy(elementData, index, newArr, index + 1, elementData.length - index);
            elementData = newArr;
        } else {
            Object[] newArr = new Object[index + 1];
            System.arraycopy(elementData, 0, newArr, 0, elementData.length);
            newArr[index] = element;
            elementData = newArr;
        }
    }

    @Override
    public Object remove(int index) {
        if (index < elementData.length) {
            Object[] newArr = new Object[elementData.length - 1];
            System.arraycopy(elementData, 0, newArr, 0, index);
            System.arraycopy(elementData, index + 1, newArr, index, elementData.length - index - 1);
            elementData = newArr;
        }
        return elementData;
    }

    @Override
    public int indexOf(Object o) {
        for (int i = 0; i < elementData.length; i++)
            if (elementData[i].equals(o))
                return i;
        return -1;
    }

    @Override
    public int lastIndexOf(Object o) {
        for (int i = elementData.length; i >= 0; i--)
            if (elementData[i].equals(o))
                return i;
        return -1;
    }

    @Override
    public ListIterator listIterator() {
        return new ListItr();
    }

    @Override
    public ListIterator listIterator(int index) {
        if (index < 0 || index > size())
            throw new IndexOutOfBoundsException("Index: " + index);
        return new ListItr();
    }

    private class Itr implements Iterator {
        int cursor = 0;

        public boolean hasNext() {
            return cursor < elementData.length;
        }

        public Object next() {
            return elementData[cursor + 1];
        }
    }

    private class ListItr extends Itr implements ListIterator {
        int cursor = 0;

        @Override
        public boolean hasPrevious() {
            return cursor > 0;
        }

        @Override
        public int nextIndex() {
            return cursor + 1;
        }

        @Override
        public int previousIndex() {
            return cursor - 1;
        }

        @Override
        public void remove() {
            Object[] newArr = new Object[elementData.length - 1];
            System.arraycopy(elementData, 0, newArr, 0, cursor);
            System.arraycopy(elementData, cursor + 1, newArr, cursor, elementData.length - cursor - 1);
            elementData = newArr;
        }

        @Override
        public void set(Object o) {
            elementData[cursor] = o;
        }

        @Override
        public void add(Object o) {
            Object[] newArr = new Object[elementData.length + 1];
            System.arraycopy(elementData, 0, newArr, 0, cursor);
            newArr[cursor] = o;
            System.arraycopy(elementData, cursor, newArr, cursor + 1, elementData.length - cursor);
            elementData = newArr;
        }

        public Object previous() {
            return elementData[cursor - 1];
        }
    }

    @Override
    public List subList(int fromIndex, int toIndex) {
        Object[] newArr = new Object[toIndex - fromIndex];
        System.arraycopy(elementData, fromIndex, newArr, 0, toIndex - fromIndex);
        return Arrays.asList(newArr);
    }

}
