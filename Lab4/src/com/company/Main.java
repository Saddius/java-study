package com.company;

import java.util.*;

public class Main {

    public static void main(String[] args) {
        int numberOfCars = 6; // Количество участников
        int[][] ArraySort = new int[numberOfCars][2];
// Создаем список объектов
        ArrayList<Vehicle> listOfCars = new ArrayList<>();
        listOfCars.add(new Car1(100, 2, 0.8));
        listOfCars.add(new Car1(100, 4, 0.2));
        listOfCars.add(new Car2(500, 2, 0.6));
        listOfCars.add(new Car2(100, 5, 0.3));
        listOfCars.add(new Car3(100, 3, 0.2));
        listOfCars.add(new Car3(100, 2, 0.5));

        for (Vehicle cars : listOfCars) {
            cars.race();
        }
        //начало сортировки
        for (int i = 0; i < ArraySort.length; i++) {
            ArraySort[i][0] = listOfCars.get(i).getTotalTime();
            ArraySort[i][1] = i + 1;
        }

        for (int i = ArraySort.length - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                if (ArraySort[j][0] > ArraySort[j + 1][0]) {
                    int tmp = ArraySort[j][0];
                    int index = ArraySort[j][1];
                    ArraySort[j][0] = ArraySort[j + 1][0];
                    ArraySort[j][1] = ArraySort[j + 1][1];
                    ArraySort[j + 1][0] = tmp;
                    ArraySort[j + 1][1] = index;
                }
            }
        }
        for (int i = 0; i < ArraySort.length; i++) {
            System.out.println("Дистанция пройдена " + ArraySort[i][1] + "-й машиной за " + ArraySort[i][0] / 60 + " мин." + (ArraySort[i][0] - ArraySort[i][0] / 60 * 60) + " сек.");
        }


    }
}
