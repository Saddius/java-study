package com.company;

/**
 * Created by Денис on 13.04.2016.
 */
abstract class Vehicle implements AutoInterface {

    public static final int dist = 2000; //длина куска трассы в метрах
    public int quantityDist = 20; //колличество кусков трассы
    private double Vmax, Vstart = 0, Acc, Vend, Mobil; // переменные максимальной скорости, начальной скорости, ускорение, конечной скорости и маневренности
    private int Time, totalTime;

    public Vehicle(double Vmax, double Acc, double Mobil) {
        this.Vmax = Vmax / 3.6;
        this.Acc = Acc;
        this.Mobil = Mobil;
    }
// метод, описывающий движение
    public void move() {
        setVend(Math.sqrt(2 * dist *getAcc() + getVstart() * getVstart()));

        if (getVend() >= getVmax()) {
            setVend(getVmax());
            int s1 = (int) ((getVend() * getVend() - getVstart() * getVstart()) / (2 * getAcc()));
            int s2 = dist - s1;
            int t1 = (int) (2 * s1 / (getVend() + getVstart()));
            int t2 = (int) (s2 / getVend());
            setTime(t1 + t2);
        } else {
            setTime((int) (2 * dist / (getVend() + getVstart())));
        }
        setTotalTime(getTotalTime() + getTime());
    }
//Сама гонка
    public void race() {

        for (int i = 1; i <= quantityDist; i++) {
            move();
            turn();

        }

    }

    public double getVend() {
        return Vend;
    }

    public void setVend(double Vend) {
        this.Vend = Vend;
    }

    public double getVmax() {
        return Vmax;
    }

    public void setVmax(double Vmax) {
        this.Vmax = Vmax;
    }

    public double getVstart() {
        return Vstart;
    }

    public void setVstart(double Vstart) {
        this.Vstart = Vstart;
    }

    public double getAcc() {
        return Acc;
    }

    public void setAcc(double Acc) {
        this.Acc = Acc;
    }

    public int getTime() {
        return Time;
    }

    public void setTime(int Time) {
        this.Time = Time;
    }

    public double getMobil() {
        return Mobil;
    }

    public void setMobil(double Mobil) {
        this.Mobil = Mobil;
    }

    public int getTotalTime() {
        return totalTime;
    }

    public void setTotalTime(int totalTime) {
        this.totalTime = totalTime;
    }

}
