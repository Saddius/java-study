package com.company;

/**
 * Created by Денис on 16.04.2016.
 */
public class Car3 extends Vehicle {
    public Car3(double Vmax, double Acc, double Mobil) {
        super(Vmax, Acc, Mobil);
    }
    @Override
    public void turn() {
        if (getVend() == getVmax()) {
            setVmax(getVmax() * 1.1);
        }
    }
}
