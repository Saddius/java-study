package com.company;

/**
 * Created by Денис on 16.04.2016.
 */
public class Car2 extends Vehicle {
    public Car2 (double Vmax, double Acc, double Mobil) {
        super(Vmax, Acc, Mobil);
    }
    double standartAcc=getAcc(); //сохраняем стандартное ускорение, которое введено при создании объекта

    @Override
    public void turn() {
        setAcc(standartAcc);
        setVstart(getVend() * getMobil());
        if (getVstart() < getVmax() / 2) {
            setAcc(getAcc() * 2);
        }
    }

}
