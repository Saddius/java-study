package com.company;
/*Вводить с клавиатуры числа и считать их сумму, пока пользователь не введёт слово «сумма». Вывести на экран полученную сумму.
*/

import java.util.*;

public class Main {

    public static void main(String[] args) {
        String inputValue = "0";
        Scanner second = new Scanner(System.in);
        float summ = 0;
        System.out.println("Ваши числа будут суммироваться пока вы не введете слово \"Сумма\"");
        do {
            try {
                summ += Float.parseFloat(inputValue);
                System.out.println("Введите число");
                inputValue = second.nextLine();
            } catch (NumberFormatException e) {
                System.out.println("Введите число");
                inputValue = second.nextLine();
            }
            } while (!inputValue.toLowerCase().equals("сумма")) ;

            if ((summ - (int) summ) > 0) {
                System.out.println("Сумма равна: " + summ);
            } else {
                System.out.println("Сумма равна: " + (int) summ);
            }
        }
    }
