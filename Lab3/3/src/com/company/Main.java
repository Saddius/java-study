package com.company;

import java.util.*;

/*Вывести все простые числа от 1 до N. N задается пользователем через консоль*/
public class Main {

    public static void main(String[] args) {
        System.out.println("Введите число, определяющее границу диапазона для нахождения простых чисел.");
        Scanner number = new Scanner(System.in);
        int quant = number.nextInt();

        int[] nums = new int[quant + 1];
        for (int i = 0; i < quant + 1; i++) {
            nums[i] = i;
        }

        nums[1] = 0;
        for (int k = 2; k < quant; k++) {
            if (nums[k] != 0) {
                for (int j = k * 2; j < quant; j += k) {
                    nums[j] = 0;
                }
            }
        }
        System.out.println("Вот все простые числа в вашем диапазоне");
        for (int i = 0; i < quant; i++) {
            if (nums[i] != 0) {
                System.out.println(nums[i]);
            }
        }

    }

}
