package com.company;
/*Дан массив из 4 чисел типа int. Сравнить их и вывести наименьшее на консоль.
*/
import java.util.*;

public class Main {

    public static void main(String[] args) {


        System.out.println("Введите целое число, минимально возможное значение массива");
        Scanner first = new Scanner(System.in);
        int min = first.nextInt();
        System.out.println("Введите целое число, максимально возможное значение массива");
        Scanner second = new Scanner(System.in);
        int max = second.nextInt();
        if (max > min) {
            int[] mass = new int[4];
            for (int i = 0; i < 4; i++) {
                mass[i] = min + (int) (Math.random() * ((max - min) + 1));
                System.out.println((i + 1) + "-й элемент массива = " + mass[i]);
            }
            Arrays.sort(mass);
            System.out.println("Минимальное значение массива = " + mass[0]);

        } else {
            System.out.println("Максимальное число должно быть больше минимального.");
        }
    }

}

