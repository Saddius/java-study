package com.company;
/*При помощи цикла for вывести на экран нечетные числа от 1 до 99.
*/

import java.util.*;

public class Main {

    public static void main(String[] args) {

        int min, max;
        try {
            System.out.println("Введите целое число, начальное для выбора нечетных чисел");
            Scanner first = new Scanner(System.in);
            min = first.nextInt();
            System.out.println("Введите целое число, конечное для выбора нечетных числе");
            Scanner second = new Scanner(System.in);
            max = second.nextInt();
        } catch (InputMismatchException e) {
            System.out.println("Ну просили же целое число, а теперь программа забирает управление на себя и теперь она поглотит весь мир или выведет нечетные числа от 1 до 99 :)");
            min = 1;
            max = 99;
        }
        if (min % 2 != 0) {
            for (int i = min; i <= max; i = i + 2) {
                System.out.println(i);
            }
        } else {
            for (int i = min + 1; i <= max; i = i + 2) {
                System.out.println(i);
            }

        }
    }
}
