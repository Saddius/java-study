package StackOverFlow.Steps;

import Rozetka.Runner.Runner;
import StackOverFlow.Runner.RunnerSt;
import StackOverFlow.StackQuestionPage;
import StackOverFlow.StackSignUpPage;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static Rozetka.Runner.Runner.driver;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by Денис on 04.06.2016.
 */
public class MyStepdefs {
    public StackSignUpPage signUpPage;
    public StackQuestionPage questionPage;

    @Given("^I open Stackoverflow start page$")
    public void iOpenStackoverflowStartPage() throws Throwable {
        RunnerSt.driver.get("http://stackoverflow.com/");
    }

    @When("^I find featured value$")
    public void iFindFeaturedValue() throws Throwable {
        assertNotNull("There is no Featured filed", RunnerSt.startPage.featuredNumber);
    }

    @Then("^I check that the value is more than (\\d+)$")
    public void iCheckThatTheValueIsMoreThan(int featuredValue) throws Throwable {
        String textName = RunnerSt.startPage.featuredNumber.getText();
        int realName = Integer.parseInt(textName);
        assertTrue("The featured value is not more than " + featuredValue, realName > featuredValue);
    }

    @When("^I click on Sign up link$")
    public void iClickOnSignUpLink() throws Throwable {
        signUpPage = RunnerSt.startPage.SignUpClick();
    }

    @And("^SignUp page is opened$")
    public void signupPageIsOpened() throws Throwable {
        assertNotNull("It's not the SignUp page", signUpPage.signUpPageExist);
    }

    @Then("^Google and Facebook links exist$")
    public void googleAndFacebookLinksExist() throws Throwable {
        List<String> signUpText = new ArrayList<String>();
        List<WebElement> signUpLinks = signUpPage.googleAndFbLink;
        for (int i = 0; i < signUpLinks.size(); i++) {
            signUpText.add(signUpLinks.get(i).getText());
        }
        assertTrue("No Google", signUpText.contains("Google"));
        assertTrue("No Facebook", signUpText.contains("Facebook"));
    }

    @When("^I click on one of Top Questions$")
    public void iClickOnOneOfTopQuestions() throws Throwable {
       questionPage = RunnerSt.startPage.clickOnQuestion();
    }

    @Then("^I check that it's created today$")
    public void iCheckThatItSCreatedToday() throws Throwable {
        assertNotNull("Question is asked not today", questionPage.dateOfCreation);
    }

    @When("^I observe the Stackoverflow page$")
    public void iObserveTheStackoverflowPage() throws Throwable {
        assertNotNull("It's not StackOverFlow page", RunnerSt.startPage.stackLogo);
    }

    @Then("^I can see salary adv with salary more than 100k$")
    public void iCanSeeSalaryAdvWithSalaryMoreThanK() throws Throwable {
        List<WebElement> dollars100 = RunnerSt.startPage.advList;
        String allAdvs = "";
        for (int i = 0; i < dollars100.size(); i++) {
            allAdvs = allAdvs + dollars100.get(i).getText();
        }
        Pattern myPat = Pattern.compile("[1][0][1-9]|[2-9][0-9][0-9]|[1][1-9][0-9]");
        Matcher matcher = myPat.matcher(allAdvs);
        assertTrue("Can't find salary higher than $100k", matcher.find());
    }
}
