package StackOverFlow.Runner;

import StackOverFlow.StackStartPage;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

/**
 * Created by Денис on 04.06.2016.
 */
@RunWith(Cucumber.class)

@CucumberOptions(
        plugin = {"html:target/cucumber-report/Stack", "json:target/cucumber.json"},
        features = "src/test/java/StackOverFlow/features",
        glue = "StackOverFlow/Steps",
        tags = "@StackTest")
public class RunnerSt {
    public static WebDriver driver;
    public static StackStartPage startPage;
    @BeforeClass
    public static void beforeClass() {
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        startPage = new StackStartPage(driver);
    }

    @AfterClass
    public static void afterClass() {
        driver.close();
    }
}
