@StackTest
Feature: Test Stack

  @Test1
  Scenario: Check that featured value is more than 300
    Given I open Stackoverflow start page
    When I find featured value
    Then I check that the value is more than 300

  @Test2
  Scenario: Check that Facebook and Google buttons are displayed on SignUp page
    Given I open Stackoverflow start page
    When I click on Sign up link
    And SignUp page is opened
    Then Google and Facebook links exist

  @Test3
  Scenario: Check that a question is created today
    Given I open Stackoverflow start page
    When I click on one of Top Questions
    Then I check that it's created today

  @Test4
  Scenario: Check that adv with salary more than 100k exist
    Given I open Stackoverflow start page
    When I observe the Stackoverflow page
    Then I can see salary adv with salary more than 100k