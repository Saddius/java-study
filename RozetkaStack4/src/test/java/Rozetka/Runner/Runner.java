package Rozetka.Runner;

import Rozetka.RozetkaMainPage;
import Rozetka.RozetkaCartPage;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

/**
 * Created by Денис on 02.06.2016.
 */
@RunWith(Cucumber.class)

@CucumberOptions(
        plugin = {"html:target/cucumber-report/Rozetka", "json:target/cucumber.json"},
        features = "src/test/java/Rozetka/features",
        glue = "Rozetka/Steps",
        tags = "@RozetkaTest")
public class Runner {
    public static WebDriver driver;
    public static RozetkaMainPage mainPage;


    @BeforeClass
    public static void beforeClass() {
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        mainPage = new RozetkaMainPage(driver);
    }

    @AfterClass
    public static void afterClass() {
        driver.close();
    }
}
