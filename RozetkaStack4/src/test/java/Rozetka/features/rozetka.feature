@RozetkaTest
Feature: Test Rozetka

  @Test1
  Scenario: Check that logo exists
    Given I open Rozetka start page
    When I observe Rozetka start page
    Then I see the logo

  @Test2
  Scenario: Check that Apple menu item exists
    Given I open Rozetka start page
    When I observe catalog list
    Then I see the Apple menu item

  @Test3
  Scenario: Check that MP3 section exists
    Given I open Rozetka start page
    When I observe catalog list
    Then I see the MP section

  @Test4
  Scenario: Check that 3 cities exist upon selecting a city
    Given I open Rozetka start page
    When I click on Select city
    Then I see Kharkiv, Odessa and Kiev in the list

  @Test5
  Scenario: Check that cart is empty
    Given I open Rozetka start page
    When I click on Cart button
    Then Cart page is open with empty Cart