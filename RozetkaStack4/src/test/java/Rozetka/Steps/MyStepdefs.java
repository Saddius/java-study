package Rozetka.Steps;

import Rozetka.RozetkaCartPage;
import Rozetka.Runner.Runner;
import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by Денис on 02.06.2016.
 */
public class MyStepdefs {
    // WebDriver driver;
    RozetkaCartPage cartPage;

    @Given("^I open Rozetka start page$")
    public void iOpenRozetkaStartPage() throws Throwable {
        Runner.driver.get("http://rozetka.com.ua/");

    }

    @When("^I observe Rozetka start page$")
    public void iObserveRozetkaStartPage() throws Throwable {
        assertNotNull("It's not Rozetka page", Runner.mainPage.inputBox);
    }

    @Then("^I see the logo$")
    public void iSeeTheLogo() throws Throwable {
        assertTrue("rozetka logo is not displayed", Runner.mainPage.logoLocation.isDisplayed());
    }

    @When("^I observe catalog list$")
    public void iObserveCatalogList() throws Throwable {
        assertNotNull("Catalog list doesn't exist", Runner.mainPage.catalogList);
    }

    @Then("^I see the Apple menu item$")
    public void iSeeTheAppleMenuItem() throws Throwable {
        assertNotNull("Can't find Apple link", Runner.mainPage.appleItem);
    }

    @Then("^I see the MP section$")
    public void iSeeTheMPSection() throws Throwable {
        assertNotNull("Can't find section containing MP3", Runner.mainPage.mp3Item);
    }

    @When("^I click on Select city$")
    public void iClickOnSelectCity() throws Throwable {
        Runner.mainPage.ClickOnCity();
    /*    if (Runner.mainPage.rejectButton != null)
            Runner.mainPage.rejectButton.click();
        Runner.mainPage.cityLink.click(); */
    }

    @Then("^I see Kharkiv, Odessa and Kiev in the list$")
    public void iSeeKharkivOdessaAndKievInTheList() throws Throwable {
        List<WebElement> chooseCitiesLnk = Runner.mainPage.chooseCitiesLnk;
        List<String> cityNames = new ArrayList<String>();
        for (int i = 0; i < chooseCitiesLnk.size(); i++) {
            cityNames.add(chooseCitiesLnk.get(i).getText());
        }
        assertTrue("No Kharkiv", cityNames.contains("Харьков"));
        assertTrue("No Kiev", cityNames.contains("Киев"));
        assertTrue("No Odessa", cityNames.contains("Одесса"));
    }

    @When("^I click on Cart button$")
    public void iClickOnCartButton() throws Throwable {
        cartPage = Runner.mainPage.ClickOnCart();
    }

    @Then("^Cart page is open with empty Cart$")
    public void cartPageIsOpenWithEmptyCart() throws Throwable {
        assertNotNull("The cart is not empty", cartPage.emptyCartCaption);
    }
}
