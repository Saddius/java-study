package StackOverFlow;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

/**
 * Created by Денис on 31.05.2016.
 */
public class StackSignUpPage {
    private WebDriver driver;
    public StackSignUpPage(WebDriver driver){
        PageFactory.initElements(driver,this);
        this.driver=driver;
    }

    @FindBy (xpath=".//a[@class='youarehere' and text()='Sign up']")
    public WebElement signUpPageExist;

    @FindBys({@FindBy(xpath = ".//div[contains(@class,'major-provider')]//span[not(@*)]")})
    public List<WebElement> googleAndFbLink;

}
