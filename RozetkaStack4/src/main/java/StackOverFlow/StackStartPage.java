package StackOverFlow;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

/**
 * Created by Денис on 31.05.2016.
 */
public class StackStartPage {
    private WebDriver driver;
    public StackStartPage(WebDriver driver){
        PageFactory.initElements(driver,this);
        this.driver=driver;
    }
    @FindBy(xpath=".//span[@class='bounty-indicator-tab']")
    public WebElement featuredNumber;

    @FindBy(xpath=".//a[text()='sign up']")
    public WebElement signUpLink;

    @FindBy (xpath=".//a[@class='question-hyperlink']")
    public WebElement questionLink;

    @FindBys({@FindBy(xpath=".//div[@class='job-info']/div[@class='title']|.//a[@data-tz='3']/div[@class='title']")})
    public List<WebElement> advList;

    @FindBy (xpath = ".//a[contains(text(),'Stack Overflow')]")
    public WebElement stackLogo;

    public StackQuestionPage clickOnQuestion() throws Exception{
     questionLink.click();
        return new StackQuestionPage(driver);
    }

    public StackSignUpPage SignUpClick() throws Exception{
        signUpLink.click();
        return new StackSignUpPage(driver);
    }

}
