package StackOverFlow;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by Денис on 01.06.2016.
 */
public class StackQuestionPage {
    private WebDriver driver;
    public StackQuestionPage(WebDriver driver){
        PageFactory.initElements(driver,this);
        this.driver=driver;
    }

     @FindBy(xpath=".//p[@class='label-key']/b[text()='today']")
    public WebElement dateOfCreation;
}
