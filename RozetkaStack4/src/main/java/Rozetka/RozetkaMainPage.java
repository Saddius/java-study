package Rozetka;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

/**
 * Created by Денис on 30.05.2016.
 */
public class RozetkaMainPage {
    private WebDriver driver;

    public RozetkaMainPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(xpath = ".//input[contains(@class,'header-search-input-text')]")
    public WebElement inputBox;

    @FindBy(xpath = "//div[@class='logo']/img")
    public WebElement logoLocation;

    @FindBy(xpath = ".//a[@id='fat_menu_btn']/span")
    public WebElement catalogList;

    @FindBy(xpath = ".//li[@menu_id='2195']/a")
    public WebElement appleItem;

    @FindBy(xpath = ".//ul[@id='m-main']/li[contains(a,'MP3')]")
    public WebElement mp3Item;

    @FindBy(xpath = ".//a[@name='city']")
    public WebElement cityLink;

    @FindBys({@FindBy(xpath = ".//div[@class='header-city-i']/a|.//div[@class='header-city-i']/span")})
    public List<WebElement> chooseCitiesLnk;

    @FindBy(xpath = ".//div[@class='notificationPanelBlock']")
    public WebElement rejectButton;

    @FindBy(xpath = ".//a[contains(@href,'cart')]")
    public WebElement cartLink;

    public RozetkaMainPage ClickOnCity() throws Exception {
        if (rejectButton != null)
            rejectButton.click();
        cityLink.click();
        return new RozetkaMainPage(driver);
    }

    public RozetkaCartPage ClickOnCart() {
        cartLink.click();
        return new RozetkaCartPage(driver);
    }

}
