import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.ArrayList;
import java.util.List;

import java.util.regex.*;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by Денис on 26.05.2016.
 */
public class TestStackoverflow {
    WebDriver driver;

    @BeforeClass
    public static void Registration() throws Exception{
        System.out.println("Здесь будет когда-нибудь регистрация");
    }

    @Before
    public void setUp() throws Exception {
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.get("http://stackoverflow.com");
    }

    @Test
    public void NumberOfFeatured() throws Exception {
        WebElement number = driver.findElement(By.xpath(".//span[@class='bounty-indicator-tab']"));
        String textName = number.getText();
        int realName = Integer.parseInt(textName);
        assertTrue("Not more than 300", realName > 300);
    }

    @Test
    public void SingUpLinks() throws Exception {
        WebElement signUpLnk = driver.findElement(By.xpath(".//a[text()='sign up']"));
        signUpLnk.click();
        List<String> signUpText = new ArrayList<String>();
        List<WebElement> signUpLinks = driver.findElements(By.xpath(".//div[contains(@class,'major-provider')]//span[not(@*)]"));
        for (int i = 0; i < signUpLinks.size(); i++) {
            signUpText.add(signUpLinks.get(i).getText());
        }
        assertTrue("No Google", signUpText.contains("Google"));
        assertTrue("No Facebook", signUpText.contains("Facebook"));
    }

    @Test
    public void TodaysQuestion() throws Exception {
        WebElement questionLnk = driver.findElement(By.xpath(".//a[@class='question-hyperlink']"));
        questionLnk.click();
        assertNotNull("Question is asked not today", driver.findElement(By.xpath(".//p[@class='label-key']/b[text()='today']")));
    }

    //  .//a[@data-tz='3']/div[@class='title']
    //.//ul[@class='jobs']//a[contains(div,'$100k') or contains(div,'$100K')]
    // .//div[@class='job-info']/div[@class='title']
    @Test
    public void salary100() throws Exception {
        List<WebElement> dollars100 = driver.findElements(By.xpath(".//div[@class='job-info']/div[@class='title']|.//a[@data-tz='3']/div[@class='title']"));
        String allAdvs = "";
        for (int i = 0; i < dollars100.size(); i++) {
            allAdvs = allAdvs + dollars100.get(i).getText();
        }
        Pattern myPat = Pattern.compile("[1][0][1-9]|[2-9][0-9][0-9]|[1][1-9][0-9]");
        Matcher matcher = myPat.matcher(allAdvs);
        assertTrue("Can't find salary higher than $100k", matcher.find());
    }

    @After
    public void afterMethod() throws Exception {
        driver.close();
    }

    @AfterClass
    public static void theAfterAfter() throws Exception{
        System.out.println("Здесь будут репорты");
    }
}


  /*   @Test
      public void GoogleLnk() throws Exception {
          WebElement signUpLnk = driver.findElement(By.xpath(".//a[text()='sign up']"));
          signUpLnk.click();
          assertNotNull("Can't find Google button", driver.findElement(By.xpath(".//span[text()='Google']")));
      }

      @Test
      public void FacebookLnk() throws Exception {
          WebElement signUpLnk = driver.findElement(By.xpath(".//a[text()='sign up']"));
          signUpLnk.click();
          assertNotNull("Can't find Google button", driver.findElement(By.xpath(".//span[text()='Facebook']")));
      } */