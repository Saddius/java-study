import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by Денис on 25.05.2016.
 */
public class TestRozetka {

    WebDriver driver;
    @BeforeClass
    public static void Registration() throws Exception{
        System.out.println("Здесь будет когда-нибудь регистрация");
    }
    @Before
    public void setUp() throws Exception {
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.get("http://rozetka.com.ua");
    }

    @Test
    public void LogoExist() throws Exception {
        WebElement lnkLogo = driver.findElement(By.xpath("//div[@class='logo']/img"));
        assertNotNull("Can't find logo", lnkLogo);
    }

    @Test
    public void AppleMenu() throws Exception {
        WebElement lnkApple = driver.findElement(By.xpath(".//li[@menu_id='2195']/a"));
        assertNotNull("Can't find Apple link", lnkApple);
    }

    @Test
    public void mp3() throws Exception {
        //.//li[@menu_id='47']/a[contains(text(),'MP3')]
        //.//li[@menu_id='47']/div//a[contains(text(),'MP3')]
        WebElement sectionMp3 = driver.findElement(By.xpath(".//ul[@id='m-main']/li[contains(a,'MP3')]"));
        assertNotNull("Can't find section containing MP3", sectionMp3);
    }

    @Test
    public void allCities() throws Exception {
        WebElement city = driver.findElement(By.xpath(".//a[@name='city']"));
        city.click();
        List<String> cityNames = new ArrayList<String>();
        List<WebElement> cityLinks = driver.findElements(By.xpath(".//div[@class='header-city-i']/a|.//div[@class='header-city-i']/span"));
        for (int i = 0; i < cityLinks.size(); i++) {
            cityNames.add(cityLinks.get(i).getText());
        }
        assertTrue("No Kharkiv", cityNames.contains("Харьков"));
        assertTrue("No Kiev", cityNames.contains("Киев"));
        assertTrue("No Odessa", cityNames.contains("Одесса"));
    }

    @Test
    public void cartEmpty() throws Exception {
        WebElement city = driver.findElement(By.xpath(".//a[contains(@href,'cart')]"));
        city.click();
        WebElement emptyCart = driver.findElement(By.xpath(".//h2[text()='Корзина пуста']"));
        assertNotNull("The cart is not empty", emptyCart);
    }

    @After
    public void afterMethod() throws Exception {
        driver.close();
    }

    @AfterClass
    public static void theAfterAfter() throws Exception{
        System.out.println("Здесь будут репорты");
    }
}



  /*  @Test
    public void cityKharkiv() throws Exception {
        WebElement city = driver.findElement(By.xpath(".//a[@name='city']"));
        city.click();
        WebElement Kharkiv = driver.findElement(By.xpath(".//a[@locality_id='31']|.//span[@locality_id='31']"));
        assertNotNull("Can't find Kharkiv", Kharkiv);
    }

    @Test
    public void cityKiev() throws Exception {
        WebElement city = driver.findElement(By.xpath(".//a[@name='city']"));
        city.click();
        WebElement Kiev = driver.findElement(By.xpath(".//a[@locality_id='1']|.//span[@locality_id='1']"));
        assertNotNull("Can't find Kiev", Kiev);
    }

    @Test
    public void cityOdessa() throws Exception {
        WebElement city = driver.findElement(By.xpath(".//a[@name='city']"));
        city.click();
        WebElement Odessa = driver.findElement(By.xpath(".//a[@locality_id='30']|.//span[@locality_id='30']"));
        assertNotNull("Can't find Odessa", Odessa);
    } */