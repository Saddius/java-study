import org.junit.Assert;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import java.net.URL;

/**
 * Created by saddius on 6/17/2016.
 */
public class attempt3 {
    public void handleMethod(String urlFromFile) {
        try {

            String[] firstUrl = urlFromFile.split("   ", 2);
            URL url = new URL(firstUrl[0]);
            DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document doc = documentBuilder.parse(url.openStream());

            doc.getDocumentElement().normalize();

            NodeList nodeList = doc.getElementsByTagName("location");
            Node node = nodeList.item(0);
            NamedNodeMap attributes = node.getAttributes();
            //получаем значение атрибут latitude
            Node latitude = attributes.getNamedItem("latitude");
            // ... и его значение
            String valueLatitude = latitude.getNodeValue();

            Assert.assertEquals("Tha values don't match", firstUrl[1], valueLatitude);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}

