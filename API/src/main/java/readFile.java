import java.io.*;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by Денис on 15.06.2016.
 */
public class readFile {
    public List<String> readFile() {
        List<String> sb = new ArrayList();
        try {
            File aFile = new File("fileWithURL.txt");
            BufferedReader fromFile = new BufferedReader(new FileReader(aFile.getAbsoluteFile()));
            try {
                String s;

                while ((s = fromFile.readLine()) != null) {
                    sb.add(s);
                }
                fromFile.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }

        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }

        return sb;
    }
}
