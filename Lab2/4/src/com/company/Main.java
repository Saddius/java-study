package com.company;
import java.util.*;

public class Main {

    public static void main(String[] args) {
        float a=0, b=0, x=0;
        System.out.println ("Введите первое число и нажмите Enter. Дробное число вводится с точкой.");
        Scanner first =  new Scanner (System.in);
        a =  first.nextFloat();
        System.out.println ("Введите один математический символ: +, -, /, *, % и нажмите Enter");
        Scanner sign =  new Scanner (System.in);
        String s = sign.next();
        System.out.println ("Введите второе число и нажмите Enter. Дробное число вводится с точкой.");
        Scanner second =  new Scanner (System.in);
        b =  second.nextFloat();
        switch (s){
            case "+": x=a+b;
                if ((x - (int)x)>0) {
                    System.out.println("Сумма равна: " + x);
                }else{
                    System.out.println("Сумма равна: " + (int) x);
                }
               break;
            case "-": x=a-b;
                if ((x - (int)x)>0) {
                    System.out.println("Разница равна: " + x);
                }else{
                    System.out.println("Разница равна: " + (int) x);
                }
                break;
            case "*": x=a*b;
                if ((x - (int)x)>0) {
                    System.out.println("Произведение равно: " + x);
                }else {
                    System.out.println("Произведение равно: " + (int) x);
                }
                break;
            case "/": x=a/b;
                if ((x - (int)x)>0) {
                    System.out.println("Частное равно: " + x);
                }else {
                    System.out.println("Частное равно: " + (int) x);
                }
                break;
            case "%": x=a%b;
                if ((x - (int)x)>0) {
                    System.out.println("Остаток от деления равен: " + x);
                }else {
                    System.out.println("Остаток от деления равен: " + (int) x);
                }
                break;
            default: System.out.println ("Вы ввели недопустимый математический символ");
        }


    }
}
