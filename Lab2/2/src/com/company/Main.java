package com.company;


public class Main {

    public static void main(String[] args) {
	//неявное преобразование в строку
        System.out.println("Неявное преобразование.");
        System.out.println();
        int intVal=20;
        String intToStr="";
        intToStr+=intVal;
        print(intToStr);

        byte byteVal=10;
        String byteToStr=""+byteVal;
        print(byteToStr);

        short shortVal=11;
        String shortToStr=""+shortVal;
        print(shortToStr);

        long longVal=12;
        String longToStr=""+longVal;
        print(longToStr);

        double doubleVal=13;
        String doubleToStr=""+doubleVal;
        print(doubleToStr);

        char charVal='A';
        String charToStr=""+charVal;
        print(charToStr);

        boolean booleanVal=true;
        String booleanToStr=""+booleanVal;
        print(booleanToStr);

        //Явное преобразование в строку
        System.out.println();
        System.out.println("Явное преобразование.");
        System.out.println();

        int intVal2=44;
        String fromIntToStr=String.valueOf(intVal2);
        print(fromIntToStr);
        // и обратно в число
        int newValInt=Integer.parseInt(fromIntToStr);
        System.out.print("И обратно: ");
        System.out.println(newValInt);

        byte byteVal2=2;
        String fromByteToStr=String.valueOf(byteVal2);
        print(fromByteToStr);
        // и обратно в число
        short newValByte=Byte.parseByte(fromByteToStr);
        System.out.print("И обратно: ");
        System.out.println(newValByte);

        short shortVal2=3;
        String fromShortToStr=String.valueOf(shortVal2);
        print(fromShortToStr);
        // и обратно в число
        short newValShort=Short.parseShort(fromShortToStr);
        System.out.print("И обратно: ");
        System.out.println(newValShort);

        long longVal2=4;
        String fromLongToStr=String.valueOf(longVal2);
        print(fromLongToStr);
        //и обратно в лонг
        long newValLong=Long.parseLong(fromLongToStr);
        System.out.print("И обратно: ");
        System.out.println(newValLong);

        double doubleVal2=5;
        String fromDoubleToStr=String.valueOf(doubleVal2);
        print(fromDoubleToStr);
        //и обратно в дабл
        double newValDouble=Double.parseDouble(fromDoubleToStr);
        System.out.print("И обратно: ");
        System.out.println(newValDouble);

        char charVal2='C';
        String fromCharToStr=String.valueOf(charVal2);
        print(fromCharToStr);
        //обратно в символ
        char newCharVal=fromCharToStr.charAt(0);
        System.out.print("И обратно: ");
        System.out.println(newCharVal);

        boolean booleanVal2=true;
        String fromBooleanToStr=String.valueOf(booleanVal2);
        print(fromBooleanToStr);
        //обратно в логическое
        boolean newBooleanVal=Boolean.parseBoolean(fromBooleanToStr);
        System.out.print("И обратно: ");
        System.out.println(newBooleanVal);
    }
    private static void print(String s1) {
        System.out.print("Преобразуем в строку: ");
        System.out.println(s1);
    }

}
