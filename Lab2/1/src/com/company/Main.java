package com.company;

public class Main {

    public static void main(String[] args) {
	 //арифметические операторы
        int a=10,z,y;
        int b=5;
        int sum=a+b;
        int mult=a*b;
        int diff=a-b;
        int div=a/b;
        int rem=a%10;
        System.out.println ("a = " +a+", b = "+b);
        System.out.println ("a + b = "+sum);
        System.out.println ("a * b = "+mult);
        System.out.println ("a - b = "+diff);
        System.out.println ("a / b = "+div);
        System.out.println ("a % b = "+rem);
        //операторы сравнения
        if (a > b) {
            System.out.println ("Значение а больше b");
        } else if (b < a) {
                System.out.println("Значение a меньше b");
            } else {
                System.out.println("Значение а равно b");
            }
        //логические операторы
        boolean truest=true, falsest=true;

        System.out.println ("Логический оператор 1 = "+truest+", Логический оператор 2 = "+falsest);
        if (truest&&falsest) {
            System.out.println ("Everything is true - Использовано логическое И");
        } else if (truest||falsest) {
            System.out.println("Something is true - использовано логическое ИЛИ");
        } else {
            System.out.println ("Nothing is true");
        }
        System.out.println ("Логическое отрицание. Логический оператор 1 = " + !truest);
        //Приравнивания
        z=20;
        y=z;
        System.out.println ("Операция приравнивания. y = "+y);
    }

}
