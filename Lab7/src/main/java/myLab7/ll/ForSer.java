package myLab7.ll;
import java.io.*;

/**
 * Created by Денис on 30.04.2016.
 */
public class ForSer implements Serializable {
    private String str = "Я клас, який можно серіалізувати";

    private void change(){
        str = "Зміна стану";
    }

    private static void Serial() throws IOException {
        FileOutputStream outStream = new FileOutputStream("myFile.out");
        ObjectOutputStream oos = new ObjectOutputStream(outStream);
        ForSer ts = new ForSer();
        oos.writeObject(ts);
        oos.flush();
        oos.close();
    }
    private String getStr(){
        return str;
    }


}
