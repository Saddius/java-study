package myLab7.ll;

import java.io.File;
import java.lang.reflect.Method;

import static org.junit.Assert.*;

/**
 * Created by Денис on 02.05.2016.
 */
public class MainTest {
     @org.junit.Test
   public void testserial() throws Exception {
        Main tst = new Main();
        tst.Serial();
        File existFile = new File("myFile.out");
        assertTrue(existFile.exists());
    }

    @org.junit.Test
    public void testUnSerial() throws Exception {
        Main tst = new Main();
        tst.unSerial();
        assertNotNull(tst);
    }

    @org.junit.Test
    public void testReflection() throws Exception {
        Main tst = new Main();
        ForSer tst2 = new ForSer();
        Method aaa = tst2.getClass().getDeclaredMethod("getStr");
        aaa.setAccessible(true);
        assertNotEquals(aaa.invoke(tst2), tst.reflection());
    }

}