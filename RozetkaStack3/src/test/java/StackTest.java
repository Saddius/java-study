import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by Денис on 31.05.2016.
 */
public class StackTest {
    WebDriver driver;
    StackStartPage mainPage;
    StackSignUpPage signUpPage;
    StackQuestionPage questionPage;

    @Before
    public void setUp() throws Exception {
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.get("http://stackoverflow.com");
        mainPage = new StackStartPage(driver);
    }

    @After
    public void endMethod() throws Exception {
        driver.close();
    }

    @Test
    public void checkFeaturedNumber() throws Exception {

        assertTrue("Not more than 300", Integer.parseInt(mainPage.featuredNumber.getText()) > 300);
    }

    @Test
    public void SignUpLinks() throws Exception {

        signUpPage = mainPage.SignUpClick();
        List<String> signUpText = new ArrayList<String>();
        for (int i = 0; i < signUpPage.googleAndFbLink.size(); i++) {
            signUpText.add(signUpPage.googleAndFbLink.get(i).getText());
        }
        assertTrue("No Google", signUpText.contains("Google"));
        assertTrue("No Facebook", signUpText.contains("Facebook"));
    }

    @Test
    public void CheckQuestionDate() throws Exception {

        questionPage = mainPage.clickOnQuestion();
        assertNotNull("Question is asked not today", questionPage.dateOfCreation);
    }

    @Test
    public void SalarySearch() throws Exception {

        String allAdvs = "";
        for (int i = 0; i < mainPage.advList.size(); i++) {
            allAdvs = allAdvs + mainPage.advList.get(i).getText();
        }
        Pattern myPat = Pattern.compile("[1][0][1-9]|[2-9][0-9][0-9]|[1][1-9][0-9]");
        Matcher matcher = myPat.matcher(allAdvs);
        assertTrue("Can't find salary higher than $100k", matcher.find());
    }


}
