import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by Денис on 30.05.2016.
 */
public class RozetkaTest {
    WebDriver driver;
    RozetkaMainPage mainPage;
    RozetkaCartPage cartPage;

    @Before
    public void setUp() throws Exception {
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.get("http://rozetka.com.ua");
        mainPage = new RozetkaMainPage(driver);
    }

    @After
    public void theEnd() throws Exception {
        driver.close();
    }

    @Test
    public void logoTest() throws Exception {
        assertNotNull("Can't find logo", mainPage.logoLocation);
    }

    @Test
    public void appleTest() throws Exception {
        assertNotNull("Can't find Apple menu item", mainPage.appleItem);
    }

    @Test
    public void mp3Test() throws Exception {
        assertNotNull("Can't find MP3 section", mainPage.mp3Item);
    }

    @Test
    public void threeCities() throws Exception {
        mainPage.ClickOnCity();
        List<String> cityNames = new ArrayList<String>();
        for (int i = 0; i < mainPage.chooseCitiesLnk.size(); i++) {
            cityNames.add(mainPage.chooseCitiesLnk.get(i).getText());
        }
        assertTrue("No Kharkiv", cityNames.contains("Харьков"));
        assertTrue("No Kiev", cityNames.contains("Киев"));
        assertTrue("No Odessa", cityNames.contains("Одесса"));
    }

    @Test
    public void emptyCartTest() throws Exception {
        cartPage = mainPage.ClickOnCart();
        assertNotNull("The cart is not empty", cartPage.emptyCartCaption);
    }


}
