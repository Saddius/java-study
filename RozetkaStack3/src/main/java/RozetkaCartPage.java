import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by Денис on 30.05.2016.
 */
public class RozetkaCartPage {
    private WebDriver driver;

    public RozetkaCartPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }
    @FindBy(xpath=".//h2[text()='Корзина пуста']")
    public WebElement emptyCartCaption;

}
